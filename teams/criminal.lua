function randomBriefCase()
local crimTeam = getTeamFromName("Criminal")
for i, player in ipairs(getPlayersInTeam(crimTeam)) do
if getElementData(player, "briefcase.status") == true then
outputChatBox("You need to deliver the previous briefcase for the next one to spawn!",player,255,255,0)
cancelEvent()
elseif getElementData(player, "briefcase.status") == false then
outputChatBox("A random briefcase has been placed in the world, go get it and deliver it for $20K",player,0,255,0)
local randomPlayer = getRandomPlayer()
local x,y,z = getElementPosition(randomPlayer)
local theBriefcase = createMarker(x+5,y,z,"cylinder",1.5,255,255,0,170,player)
local theBlip = createBlipAttachedTo(theBriefcase,52)
setElementData(player, "briefcase.status",true)
addEventHandler("onMarkerHit",theBriefcase,
function(hitElement,matchingDimension)
if getElementType(hitElement) == "player" then
local playersName = getPlayerName(hitElement)
local sourcePlayer = getPlayerFromName(playersName)
destroyElement(theBriefcase)
destroyElement(theBlip)
outputChatBox("You have picked up the briefcase now take it to the delivery point!",sourcePlayer,0,255,0)
outputChatBox("Deliver the briefcase to the dollar blip!",sourcePlayer,0,255,255)
local deliveryBlip = createBlip(2459.545,-1690.799,13.543,52,0,0,0,255)
local deliveryMarker = createMarker(2459.545,-1690.799,13.543,"cylinder",1.5,255,255,0,170,sourcePlayer)
setElementVisibleTo(deliveryBlip,root,false)
setElementVisibleTo(deliveryMarker,root,false)
setElementVisibleTo(deliveryBlip,sourcePlayer,true)
setElementVisibleTo(deliveryMarker,sourcePlayer,true)
addEventHandler("onMarkerHit",deliveryMarker,
function(hitElement,matchingDimension)
local pAcc = getPlayerAccount(hitElement)
local thePlayer = getPlayerName(hitElement)
local thePlayer2 = getPlayerFromName(thePlayer)
local criminals = getTeamFromName("Criminal")
outputChatBox("You have delivered the briefcase and earned 20,000$",thePlayer2,0,255,0)
givePlayerMoney(thePlayer2,20000)
destroyElement(deliveryBlip)
destroyElement(deliveryMarker)
local theDatha = getAccountData(pAcc, "acc.stats.briefcase")
local numberedData = tonumber(theDatha) + 1
setAccountData(pAcc, "acc.stats.briefcase",numberedData)
for i, players in ipairs(getPlayersInTeam(criminals)) do
setElementData(players, "briefcase.status",false)
outputChatBox(""..thePlayer.. " has delivered the briefcase!",players,0,255,255)
end
end)
end
end)
end
end
end
setTimer(randomBriefCase,1000000,0)