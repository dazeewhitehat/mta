function createTeamsOnStart()
    local staffTeam = createTeam("Staff", 255,255,255)
    local governmentTeam = createTeam("Government", 166,166,166 )
    local armyTeam = createTeam("Army", 170,140,140 )
	local policeTeam = createTeam("Police", 0,110,255)
	local emsTeam = createTeam("Emergency Services", 0,255,221)
	local civilianTeam = createTeam("Civilian", 242,255,0 )
	local criminalTeam = createTeam("Criminal", 255,0,0 )
end
addEventHandler("onResourceStart", resourceRoot, createTeamsOnStart)
 