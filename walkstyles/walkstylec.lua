GUIEditor = {
    gridlist = {},
    window = {},
    button = {},
    label = {}
}
    function walkOpen()
        GUIEditor.window[2] = guiCreateWindow(507, 161, 353, 447, "Spawn selection menu", false)
        guiWindowSetSizable(GUIEditor.window[2], false)
        guiSetAlpha(GUIEditor.window[2], 0.85)

        GUIEditor.gridlist[2] = guiCreateGridList(10, 31, 329, 261, false, GUIEditor.window[2])
        guiGridListAddColumn(GUIEditor.gridlist[2], "WalkStyle Selection Menu!", 0.9)
        for i = 1, 3 do
            guiGridListAddRow(GUIEditor.gridlist[2])
        end
        guiGridListSetItemText(GUIEditor.gridlist[2], 0, 1, "Default", false, false)
        guiGridListSetItemText(GUIEditor.gridlist[2], 1, 1, "Sneak", false, false)
        guiGridListSetItemText(GUIEditor.gridlist[2], 2, 1, "SWAT", false, false)
        GUIEditor.button[2] = guiCreateButton(22, 375, 50, 27, "", false, GUIEditor.gridlist[2])

        GUIEditor.button[2] = guiCreateButton(10, 302, 107, 34, "Spawn", false, GUIEditor.window[2])
        GUIEditor.button[3] = guiCreateButton(237, 302, 102, 34, "Cancel", false, GUIEditor.window[2])
        GUIEditor.label[1] = guiCreateLabel(24, 336, 246, 102, "Description of the selected style:", false, GUIEditor.window[2])
		showCursor(not isCursorShowing())
end
addEvent("openWalkMenu",true)
addEventHandler("openWalkMenu",getRootElement(),walkOpen)

addCommandHandler("walk",walkOpen)
function closeGUI (state)
 if state == "left" or "right" then
  if source == GUIEditor.button[3] then
   guiSetVisible(GUIEditor.window[2], false)
   showCursor(false)
   setElementData(source,"temp2.selected1",false)
   setElementData(source, "temp2.selected2",false)
   setElementData(source, "temp2.selected3",false)
   end
  end
 end
addEventHandler("onClientGUIClick", getRootElement(), closeGUI)
function click (state)
thePlayer = getLocalPlayer()
if state == "left" then
  if source == GUIEditor.button[2] then
       local walkOpen = guiGridListGetItemText ( GUIEditor.gridlist[2], guiGridListGetSelectedItem ( GUIEditor.gridlist[2] ), 1 )
	if walkOpen == nil or walkOpen == false then
	outputChatBox("You need to select a city!",255,255,0)
	elseif walkOpen == "Default" then
       outputChatBox("You have selected and applied the default walkstyle!!",0,255,0)
	   guiSetVisible(GUIEditor.window[2],false)
	   showCursor(false)
	   setPedWalkingStyle(thePlayer,0)
	   setElementData(thePlayer,"temp.walkOpen",0)
	elseif walkOpen == "Sneak" then
	   outputChatBox("You have selected and applied the sneak walkstyle!!",0,255,0)
	   guiSetVisible(GUIEditor.window[2],false)
	   showCursor(false)
	   setPedWalkingStyle(thePlayer,69)
	   setElementData(thePlayer, "temp.walkOpen",1)
	elseif walkOpen == "SWAT" then
	   outputChatBox("You have selected and applied the SWAT walkstyle!",0,255,0)
       guiSetVisible(GUIEditor.window[2],false)
	   showCursor(false)
	   setPedWalkingStyle(thePlayer,128)
	   setElementData(thePlayer, "temp.walkOpen",2)
	   else 
	   outputChatBox("You need to select the walkstyle!",255,255,0)
	            end
            end
        end
    end
addEventHandler ( "onClientGUIClick", getRootElement(), click )
function updateLabel(state)
if state == "left" then
   if source == GUIEditor.gridlist[2] then
   local walkOpen = guiGridListGetItemText ( GUIEditor.gridlist[2], guiGridListGetSelectedItem ( GUIEditor.gridlist[2] ), 1 )
   if getElementData(source,"temp2.selected1") == false and walkOpen == "Default" then
   GUIEditor.label[2] = guiCreateLabel(10, 20, 247, 86, "Default CJ walkstyle", false, GUIEditor.label[1])
   guiSetVisible(GUIEditor.label[3],false)
   guiSetVisible(GUIEditor.label[4],false)
   setElementData(source, "temp2.selected1",true)
   setElementData(source, "temp2.selected2",false)
   setElementData(source, "temp2.selected3",false)
  elseif getElementData(source, "temp2.selected1") == true and walkOpen == "Default" then
  outputChatBox("Already selected!",255,0,0)
  guiSetVisible(GUIEditor.label[2],false)
  guiSetVisible(GUIEditor.label[2],true)
  guiSetVisible(GUIEditor.label[3],false)
  guiSetVisible(GUIEditor.label[4],false)
  elseif getElementData(source, "temp2.selected2") == false and walkOpen == "Sneak" then
   GUIEditor.label[3] = guiCreateLabel(10, 20, 247, 86, "Can run with all weapons", false, GUIEditor.label[1])
   guiSetVisible(GUIEditor.label[2],false)
   guiSetVisible(GUIEditor.label[4],false)
   setElementData(source, "temp2.selected2",true)
   setElementData(source, "temp2.selected3",false)
   setElementData(source, "temp2.selected1",false)
  elseif getElementData(source, "temp2.selected2") == true and walkOpen == "Sneak" then
  outputChatBox("Already selected!",255,0,0)
  guiSetVisible(GUIEditor.label[3],false)
  guiSetVisible(GUIEditor.label[3],true)
  guiSetVisible(GUIEditor.label[4],false)
  guiSetVisible(GUIEditor.label[2],false)
  elseif getElementData(source, "temp2.selected3") == false and walkOpen == "SWAT" then
  GUIEditor.label[4] = guiCreateLabel(10, 20, 247, 86, "Can't be seen on the radar (cant run)", false, GUIEditor.label[1])
  guiSetVisible(GUIEditor.label[3],false)
  guiSetVisible(GUIEditor.label[2],false)
  setElementData(source, "temp2.selected3",true)
  setElementData(source, "temp2.selected2",false)
  setElementData(source, "temp2.selected1",false)
  elseif getElementData(source, "temp2.selected3") == true and walkOpen == "SWAT" then
  outputChatBox("Already selected!",255,0,0)
  guiSetVisible(GUIEditor.label[4],false)
  guiSetVisible(GUIEditor.label[4],true)
  guiSetVisible(GUIEditor.label[3],false)
  guiSetVisible(GUIEditor.label[2],false)
    end
   end
  end
 end
addEventHandler("onClientGUIClick",getRootElement(),updateLabel)
function doubleClickedLabel(state)
if state == "left" or "right" then
if source == GUIEditor.gridlist[2] then
outputChatBox("You have double clicked!",255,0,0)
guiSetVisible(GUIEditor.label[2],false)
guiSetVisible(GUIEditor.label[3],false)
guiSetVisible(GUIEditor.label[4],false)
  end
 end
end
addEventHandler("onClientGUIDoubleClick",getRootElement(),doubleClickedLabel)
function openMenu(player)
guiSetVisible(GUIEditor.window[2],true)
showCursor(true)
end