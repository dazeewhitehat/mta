function getPlayerFromPartialName(name)
    local name = name and name:gsub("#%x%x%x%x%x%x", ""):lower() or nil
    if name then
        for _, player in ipairs(getElementsByType("player")) do
            local name_ = getPlayerName(player):gsub("#%x%x%x%x%x%x", ""):lower()
            if name_:find(name, 1, true) then
                return player
            end
        end
    end
end

function markPlayer(thePlayer,commandname,player2)
playername = getPlayerFromPartialName(player2)
if player2 == nil or not playername then
outputChatBox("You have to insert a valid player name to mark them, or a part of their name!",thePlayer,255,0,0)
cancelEvent()
else
playername = getPlayerFromPartialName(player2)
playername2 = getPlayerName(playername)
playeraccount2 = getPlayerAccount(thePlayer)
playeraccount7 = getPlayerAccount(playername)
if getAccountData(playeraccount7, "settings.marking") == false then

if getAccountData(playeraccount2, "marked.latest.temp") == playername2 then
cancelEvent()
outputChatBox("" ..playername2.. " is already marked use /unmark <player's name> to remove the mark!",thePlayer, 255,0,0)
else
outputChatBox("" ..playername2..  " has been marked!",thePlayer,0,255,0)
blip1 = createBlipAttachedTo(playername,58)
setElementVisibleTo(blip1, root, false)
setElementVisibleTo(blip1, thePlayer, true)
--setElementData(thePlayer, "tempdata.marked1",true)
setAccountData(playeraccount2, "marked.latest.temp", playername2 )

    end
else
outputChatBox("" ..playername2.. " has disabled marking!",thePlayer, 255,0,0)
   end
  end
end
function checkMark(thePlayer,commandname,player3)
playername = getPlayerFromPartialName(player3)
playername4 = getPlayerName(playername)
playeraccount3 = getPlayerAccount(thePlayer)
if player3 == nil or not playername then
outputChatBox("You have to insert a player's name to unmark them, or a part of their name!",thePlayer,255,0,0)
cancelEvent()
else
if getAccountData(playeraccount3, "marked.latest.temp") == playername2 then
outputChatBox("The mark was removed from "..playername2.. "",thePlayer,255,0,0)
destroyElement(blip1)
setAccountData(playeraccount3,"marked.latest.temp",false)
elseif getAccountData(playeraccount3, "marked.latest.temp") == false then
outputChatBox("" ..playername4.. " is not marked!",thePlayer,255,0,0)
cancelEvent()
  end
 end
end
addCommandHandler("mark",markPlayer)
addCommandHandler("unmark",checkMark)