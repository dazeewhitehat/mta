function isPlayerInTeam(player, team)
    assert(isElement(player) and getElementType(player) == "player", "Bad argument 1 @ isPlayerInTeam [player expected, got " .. tostring(player) .. "]")
    assert((not team) or type(team) == "string" or (isElement(team) and getElementType(team) == "team"), "Bad argument 2 @ isPlayerInTeam [nil/string/team expected, got " .. tostring(team) .. "]")
    return getPlayerTeam(player) == (type(team) == "string" and getTeamFromName(team) or (type(team) == "userdata" and team or (getPlayerTeam(player) or true)))
end
addCommandHandler("quitjob",
function(thePlayer)
local playeracc = getPlayerAccount(thePlayer)
if isGuestAccount(playeracc) then return
outputChatBox("You have to be logged in to use this command!",thePlayer,255,0,0)
end
    local civTeam = getTeamFromName("Civilian")
	local playeraccount = getPlayerAccount(thePlayer)
	local civilianOccupation = "Civilian"
	  if civTeam and thePlayer then
	  outputChatBox("You have successfuly became a Civilian!",thePlayer,245,255,0)
         setPlayerTeam(thePlayer, civTeam)
	       setPlayerNametagColor(thePlayer, 245,255,0)
		   setAccountData(playeraccount, "account.occupation",civilianOccupation)
      end
end)
addCommandHandler("criminal",
function(thePlayer)
local playeracc = getPlayerAccount(thePlayer)
if isGuestAccount(playeracc) then return
outputChatBox("You have to be logged in to take this job!",thePlayer,255,0,0)
end
    local crimTeam = getTeamFromName("Criminal")
	local playeraccount = getPlayerAccount(thePlayer)
	   if isPlayerInTeam(thePlayer, "Police" or "Army" or "Government") then
	   outputChatBox("You must quit your current job to become a Criminal use /quitjob !",thePlayer, 245,255,0)
	   elseif isPlayerInTeam(thePlayer, "Criminal") then
	   outputChatBox("You already are a Criminal!",thePlayer,255,0,0)
	   else
	   local criminalOccupation = "Criminal"
	   outputChatBox("You are now a Criminal!",thePlayer,255,0,0)
	      setPlayerTeam(thePlayer, crimTeam)
		    setPlayerNametagColor(thePlayer, 255,0,0)
			setAccountData(playeraccount,"account.occupation",criminalOccupation)
		end
end)
addCommandHandler("police",
function(thePlayer)
local playeracc = getPlayerAccount(thePlayer)
if isGuestAccount(playeracc) then return
outputChatBox("You have to be logged in to take this job!",thePlayer,255,0,0)
end
    local policeTeam = getTeamFromName("Police")
	local playeraccount = getPlayerAccount(thePlayer)
	   if isPlayerInTeam(thePlayer, "Criminal" or "Rebels") then
	    outputChatBox("Please quit your job before becoming a Police Officer! (due to abuse) use /quitjob",thePlayer,245,255,0)
       elseif isPlayerInTeam(thePlayer, "Police") then
	   outputChatBox("You already are a Police Officer!",thePlayer,255,0,0)
	   else
	   policeOfficerOccupation = "Police Officer"
       setPlayerTeam(thePlayer,policeTeam)
       outputChatBox("You are now a Police Officer!",thePlayer,0,110,255)
	   setPlayerNametagColor(thePlayer,0,110,255)
	   setAccountData(playeraccount,"account.occupation", policeOfficerOccupation)
       end
end)
addCommandHandler("gostaff",
function(client)
local playeracc = getPlayerAccount(client)
if isGuestAccount(playeracc) then return 
outputChatBox("You have to be logged in to use /gostaff command!",client,255,0,0)
end
     local staffTeam = getTeamFromName("Staff")
	 local playeracc = getPlayerAccount(client)
     local playeraccount = getAccountName(getPlayerAccount(client))
	 if isPlayerInTeam(client,"Staff") then
	 outputChatBox("You are already have the staff job!",client,255,0,0)
	 elseif isObjectInACLGroup("user."..playeraccount, aclGetGroup("Admin")) then
	 local onDutyStaffOccupation = "Staff on duty"
	 outputChatBox("You are now on duty as staff!",client,255,255,255)
	 setPlayerTeam(client,staffTeam)
	 setAccountData(playeracc, "account.occupation", onDutyStaffOccupation)
	 setPlayerNametagColor(client,255,255,255)
	 setPedSkin(client, 217)
	 else
	 outputChatBox("You do not have permission to use /gostaff !",client,255,0,0)
   end
end)
addCommandHandler("godmode",
function(thePlayer)
    local account = getPlayerAccount ( thePlayer )
    if ( not account or isGuestAccount ( account ) ) then
        return
    end
 
    local accountName = getAccountName ( account )
    if ( isObjectInACLGroup ( "user.".. accountName, aclGetGroup ( "Admin" ) ) ) then
        local state = ( not getElementData ( thePlayer, "invincible" ) )
        setElementData ( thePlayer, "invincible", state )
        outputChatBox ( "God Mode is now ".. ( state and "Enabled" or "Disabled" ) ..".", thePlayer, 0, 255, 0 )
    end
end)