function simpleSettings(thePlayer,commandname,atr3)
playeraccount = getPlayerAccount(thePlayer)

if atr3 == nil or not "marking" then
outputChatBox("You have to insert a setting to see the list of settings do /settings list",thePlayer,0,255,0)
cancelEvent()

elseif atr3 == "marking" then

if getAccountData(playeraccount, "settings.marking") == true then
outputChatBox("You have enabled marking!",thePlayer, 0,255,0)
setAccountData(playeraccount, "settings.marking", false)
elseif getAccountData(playeraccount, "settings.marking") == false then
outputChatBox("You have disabled marking!",thePlayer, 255,0,0)
setAccountData(playeraccount, "settings.marking", true)
 end
 elseif atr3 == "sms" then
if getAccountData(playeraccount, "settings.sms") == true then
 outputChatBox("Your SMS is enabled now, others may SMS you!",thePlayer,0,255,0)
 setAccountData(playeraccount, "settings.sms",false)
 elseif getAccountData(playeraccount, "settings.sms") == false then
 outputChatBox("You have disabled your SMS, no one can SMS you now!",thePlayer,255,0,0)
 setAccountData(playeraccount, "settings.sms", true)
  end
 end
end
addCommandHandler("settings",simpleSettings)