function getPlayerFromPartialName(name)
    local name = name and name:gsub("#%x%x%x%x%x%x", ""):lower() or nil
    if name then
        for _, player in ipairs(getElementsByType("player")) do
            local name_ = getPlayerName(player):gsub("#%x%x%x%x%x%x", ""):lower()
            if name_:find(name, 1, true) then
                return player
            end
        end
    end
end
function sendSMS(thePlayer,cmdname,reciepent,...)
local smsWords = {...}
local smsMessage = table.concat(smsWords, " ")
local playerName = getPlayerName(thePlayer)
local partNameReciepent = getPlayerFromPartialName(reciepent)
local partName = getPlayerName(partNameReciepent)
local pAcc = getPlayerAccount(partNameReciepent)
if getAccountData(pAcc, "settings.sms") == false then
if reciepent == playerName then
outputChatBox("You can't SMS yourself, silly!",thePlayer,255,255,0)
cancelEvent()
end
if reciepent == nil then
outputChatBox("Correct usage is < /sms playername yourtext >!",thePlayer,255,255,0)
cancelEvent()
else
outputChatBox("" ..smsMessage.. " sent to " ..partName.. " ", thePlayer,0,255,255)
outputChatBox("" ..smsMessage.. " sent by " ..playerName.. " ", partNameReciepent,0,255,0)
setElementData(partNameReciepent, "last.sms",playerName)
  end
elseif getAccountData(pAcc, "settings.sms") == true then
outputChatBox(""..partName.. " has disabled recieving SMS's!",thePlayer,255,0,0)
end
end
function quickReply(thePlayer,cmdname,...)
local smsWords = {...}
local smsMessage = table.concat(smsWords, " ")
local lastSmsData = getElementData(thePlayer,"last.sms")
local playerName = getPlayerName(thePlayer)
local partNameReciepent = getPlayerFromName(lastSmsData)
local partName = getPlayerName(partNameReciepent)
if getElementData(thePlayer, "last.sms") == false then
outputChatBox("The last person you SMS-ed is now offline!(you probbably reconnected!)",thePlayer,255,255,0)
cancelEvent()
else
outputChatBox("" ..smsMessage.. " sent to " ..partName.. " ",thePlayer,0,255,255)
outputChatBox("" ..smsMessage.. " sent by " ..playerName.. " ",partNameReciepent,0,255,0)
end
end
addCommandHandler("sms",sendSMS)
addCommandHandler("re",quickReply)
addCommandHandler("r",quickReply)