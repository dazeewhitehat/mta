function PlayerLogin(username,password,checksave)
	if not (username == "") then
		if not (password == "") then
			local account = getAccount ( tostring(username), tostring(password) )
			if ( account ~= false ) then
				logIn(source, account, password)
				triggerClientEvent (source,"closeGUI",getRootElement())
					if checksave == true then
						triggerClientEvent(source,"saveLoginToXML",getRootElement(),username,password)
					else
						triggerClientEvent(source,"resetSaveXML",getRootElement(),username,password)
					end

			else
				outputChatBox("Wrong account name and or password!",source,0,255,255)
			end
		else
			outputChatBox("Please enter your password!",source,0,255,255)
		end
	else
		outputChatBox("Please enter your username",source,0,255,255)
	end
end
addEvent("submitLogin",true)
addEventHandler("submitLogin",getRootElement(),PlayerLogin)
function triggerJoinEvent()
triggerClientEvent(source,"openLogin",getRootElement())
end
addEventHandler("onPlayerJoin",getRootElement(),triggerJoinEvent)
function registerPlayer(username,password,passwordConfirm)
	if not (username == "") then
		if not (password == "") then
			if not (passwordConfirm == "") then
				if password == passwordConfirm then
					local account = getAccount (username,password)
					if (account == false) then
						local accountAdded = addAccount(tostring(username),tostring(password))
						if (accountAdded) then
							outputChatBox ("#FF0000* #00FF00You have sucessfuly registered! [Username: #FFFFFF" .. username .. " #00FF00| Password: #FFFFFF" .. password .. "#00FF00 ]",source,255,255,255,true )
						triggerClientEvent(source,"closeRegg",getRootElement())
						else
							outputChatBox("An unknown error has occured! Please choose a different username/password and try again.",source,0,255,255)
							triggerClientEvent(source,"closeReg",getRootElement())
						end
					else
						outputChatBox("An account with this name already exists!",source,0,255,255)
						triggerClientEvent(source,"closeReg",getRootElement())
					end
				else
					outputChatBox("Passwords don't match!",source,0,255,255)
					triggerClientEvent(source,"closeReg",getRootElement())
				end
			else
				outputChatBox("Please confirm your password!",source,0,255,255)
				triggerClientEvent(source,"closeReg",getRootElement())
			end
		else
			outputChatBox("Please enter a password!",source,0,255,255)
			triggerClientEvent(source,"closeReg",getRootElement())
		end
	else
		outputChatBox("Please enter an account name you would like to register with!",source,0,255,255)
		triggerClientEvent(source,"closeReg",getRootElement())
	end
end
addEvent("onRequestRegister",true)
addEventHandler("onRequestRegister",getRootElement(),registerPlayer)