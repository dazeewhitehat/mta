GUIEditor = {
    checkbox = {},
    edit = {},
    button = {},
    window = {},
    label = {}
}

    function loginOpen()
        GUIEditor.window[1] = guiCreateWindow(326, 85, 379, 179, "", false)
        guiWindowSetSizable(GUIEditor.window[1], false)
        guiSetAlpha(GUIEditor.window[1], 1.00)
        guiSetProperty(GUIEditor.window[1], "Alpha", "1.000000")
        GUIEditor.label[1] = guiCreateLabel(30, 63, 88, 16, "Account Name:", false, GUIEditor.window[1])
        guiSetFont(GUIEditor.label[1], "default-bold-small")
        GUIEditor.label[2] = guiCreateLabel(11, 99, 107, 15, "Account Password:", false, GUIEditor.window[1])
        guiSetFont(GUIEditor.label[2], "default-bold-small")
        GUIEditor.edit[1] = guiCreateEdit(127, 58, 114, 25, "", false, GUIEditor.window[1])
        GUIEditor.edit[2] = guiCreateEdit(127, 94, 115, 24, "", false, GUIEditor.window[1])
        guiSetProperty(GUIEditor.edit[2], "MaskText", "True")
        GUIEditor.checkbox[1] = guiCreateCheckBox(332, 61, 15, 17, "Save", false, false, GUIEditor.window[1])
		GUIEditor.label[3] = guiCreateLabel(256, 61, 71, 17, "Save Login ?", false, GUIEditor.window[1])
        guiSetFont(GUIEditor.label[3], "default-bold-small")
        GUIEditor.button[1] = guiCreateButton(16, 128, 96, 30, "Login", false, GUIEditor.window[1])
        GUIEditor.button[2] = guiCreateButton(144, 128, 88, 30, "Close", false, GUIEditor.window[1])
        GUIEditor.label[4] = guiCreateLabel(138, 25, 199, 19, "The Login Panel", false, GUIEditor.window[1])
        guiSetFont(GUIEditor.label[4], "default-bold-small")
        GUIEditor.button[3] = guiCreateButton(268, 128, 89, 30, "Register", false, GUIEditor.window[1])    
		showCursor(true)
		guiSetVisible(GUIEditor.window[1],true)
		guiSetInputEnabled(true)
		local username,password = loadLoginFromXML()
	   if not( username == "" or password == "") then
	   local decoded = base64Decode(tostring(password))
	      guiCheckBoxGetSelected(GUIEditor.checkbox[1],true)
		  guiSetText(GUIEditor.edit[1],tostring(username))
		  guiSetText(GUIEditor.edit[2],decoded)
		  guiCheckBoxSetSelected(GUIEditor.checkbox[1],true)
		  else guiCheckBoxGetSelected(GUIEditor.checkbox[1],false)
		  guiSetText(GUIEditor.edit[1],tostring(username))
		  guiSetText(GUIEditor.edit[2],decoded)
		  guiCheckBoxSetSelected(GUIEditor.checkbox[1],false)
		end
		addEventHandler("onClientGUIClick",GUIEditor.button[1],onClickBtnLogin)
		addEventHandler("onClientGUIClick",GUIEditor.button[2],onClickBtnClose)
		addEventHandler("onClientGUIClick",GUIEditor.button[3],onClickBtnRegister)
	end
addEvent("openLogin",true)
addEventHandler("openLogin",getRootElement(),loginOpen)
function hideLoginWindow()
	guiSetInputEnabled(false)
	guiSetVisible(GUIEditor.window[1], false)
	destroyElement(GUIEditor.window[1])
	showCursor(false)
end
addEvent("closeGUI",true)
addEventHandler("closeGUI",getRootElement(),hideLoginWindow)
function onClickBtnLogin(button,state)
	if(button == "left" and state == "up") then
		if (source == GUIEditor.button[1]) then
			username = guiGetText(GUIEditor.edit[1])
			password = guiGetText(GUIEditor.edit[2])
			guiSetInputEnabled(false)
				if guiCheckBoxGetSelected ( GUIEditor.checkbox[1] ) then
					checksave = true
				else
					checksave = false
				end
			triggerServerEvent("submitLogin",getLocalPlayer(),username,password,checksave)
		end
	end
end
function onClickBtnRegister(button,state)
if (button == "left" and state == "up") then
   if (source == GUIEditor.button[3]) then
   triggerServerEvent("openRegC",getLocalPlayer())
   guiSetVisible(GUIEditor.window[1], false)
   destroyElement(GUIEditor.window[1])
   guiSetInputEnabled(true)
end
end
end
function onClickBtnClose(button,state)
if (button == "left" and state == "up") then
   if (source == GUIEditor.button[2]) then
   	guiSetInputEnabled(true)
	guiSetVisible(GUIEditor.window[1], false)
	destroyElement(GUIEditor.window[1])
	showCursor(false)
	removeEventHandler("onClientGUIClick",GUIEditor.button[1],onClickBtnLogin)
	end
  end
end
function loadLoginFromXML()
	local xml_save_log_File = xmlLoadFile ("/userdata.xml")
    if not xml_save_log_File then
        xml_save_log_File = xmlCreateFile("/userdata.xml", "login")
    end
    local usernameNode = xmlFindChild (xml_save_log_File, "username", 0)
    local passwordNode = xmlFindChild (xml_save_log_File, "password", 0)
    if usernameNode and passwordNode then
        return xmlNodeGetValue(usernameNode), xmlNodeGetValue(passwordNode)
    else
		return "", ""
    end
    xmlUnloadFile ( xml_save_log_File )
end
function saveLoginToXML(username, password)
    local xml_save_log_File = xmlLoadFile ("/userdata.xml")
    if not xml_save_log_File then
        xml_save_log_File = xmlCreateFile("/userdata.xml", "login")
    end
	if (username ~= "") then
		local usernameNode = xmlFindChild (xml_save_log_File, "username", 0)
		if not usernameNode then
			usernameNode = xmlCreateChild(xml_save_log_File, "username")
		end
		xmlNodeSetValue (usernameNode, tostring(username))
	end
	if (password ~= "") then
		local passwordNode = xmlFindChild (xml_save_log_File, "password", 0)
		if not passwordNode then
			passwordNode = xmlCreateChild(xml_save_log_File, "password")
		end
		local encoded = base64Encode(tostring(password))
		xmlNodeSetValue (passwordNode, encoded)
	end
    xmlSaveFile(xml_save_log_File)
    xmlUnloadFile (xml_save_log_File)
end
addEvent("saveLoginToXML", true)
addEventHandler("saveLoginToXML", getRootElement(), saveLoginToXML)

function resetSaveXML()
		local xml_save_log_File = xmlLoadFile ("/userdata.xml")
		if not xml_save_log_File then
			xml_save_log_File = xmlCreateFile("/userdata.xml", "login")
		end
		if (username ~= "") then
			local usernameNode = xmlFindChild (xml_save_log_File, "username", 0)
			if not usernameNode then
				usernameNode = xmlCreateChild(xml_save_log_File, "username")
			end
		end
		if (password ~= "") then
			local passwordNode = xmlFindChild (xml_save_log_File, "password", 0)
			if not passwordNode then
				passwordNode = xmlCreateChild(xml_save_log_File, "password")
			end		
			xmlNodeSetValue (passwordNode, "")
		end
		xmlSaveFile(xml_save_log_File)
		xmlUnloadFile (xml_save_log_File)
end
addEvent("resetSaveXML", true)
addEventHandler("resetSaveXML", getRootElement(), resetSaveXML)