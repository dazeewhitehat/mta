
GUIEditorr = {
    edit = {},
    button = {},
    window = {},
    label = {},
    radiobutton = {}
}
function openRegisterGUI()
        GUIEditorr.window[1] = guiCreateWindow(326, 85, 417, 275, "", false)
        guiWindowSetSizable(GUIEditorr.window[1], false)
        guiSetAlpha(GUIEditorr.window[1], 1.00)
        GUIEditorr.label[1] = guiCreateLabel(30, 56, 88, 16, "Account Name:", false, GUIEditorr.window[1])
        guiSetFont(GUIEditorr.label[1], "default-bold-small")
        GUIEditorr.label[2] = guiCreateLabel(11, 86, 107, 15, "Account Password:", false, GUIEditorr.window[1])
        guiSetFont(GUIEditorr.label[2], "default-bold-small")
        GUIEditorr.edit[1] = guiCreateEdit(118, 52, 114, 25, "", false, GUIEditorr.window[1])
        GUIEditorr.edit[2] = guiCreateEdit(118, 82, 115, 24, "", false, GUIEditorr.window[1])
        GUIEditorr.button[1] = guiCreateButton(53, 232, 137, 30, "Register!", false, GUIEditorr.window[1])
        GUIEditorr.button[2] = guiCreateButton(240, 232, 118, 30, "Back", false, GUIEditorr.window[1])
        GUIEditorr.label[3] = guiCreateLabel(138, 25, 84, 17, "Registration", false, GUIEditorr.window[1])
        guiSetFont(GUIEditorr.label[3], "default-bold-small")
        GUIEditorr.label[4] = guiCreateLabel(11, 116, 105, 15, "Confirm Password:", false, GUIEditorr.window[1])
        guiSetFont(GUIEditorr.label[4], "default-bold-small")
        GUIEditorr.edit[3] = guiCreateEdit(118, 112, 116, 23, "", false, GUIEditorr.window[1])
        GUIEditorr.edit[4] = guiCreateEdit(138, 141, 220, 23, "", false, GUIEditorr.window[1])
        GUIEditorr.label[5] = guiCreateLabel(24, 145, 104, 19, "E-Mail(optional):", false, GUIEditorr.window[1])
        guiSetFont(GUIEditorr.label[5], "default-bold-small")
        GUIEditorr.label[6] = guiCreateLabel(10, 164, 338, 60, "Account name is different from your nick name,remember it!\nThe password should not be shared with anyone for safety!\nE-Mail is optional and is used to recover your password\n if you lose it at any time!", false, GUIEditorr.window[1])
        GUIEditorr.label[7] = guiCreateLabel(230, 21, 187, 110, "You will be prompted to a menu\n to select the city you want to \n spawn in for the first time.\n When you finish registration.", false, GUIEditorr.window[1])
        guiSetFont(GUIEditorr.label[7], "default-bold-small")
        guiSetProperty(GUIEditorr.edit[2], "MaskText", "True")
        guiSetProperty(GUIEditorr.edit[3], "MaskText", "True")		
		guiSetVisible(GUIEditorr.window[1],true)
        showCursor(true)
		guiSetInputEnabled(true)
		addEventHandler("onClientGUIClick",GUIEditorr.button[2],onClickBtnBack)
		addEventHandler("onClientGUIClick",GUIEditorr.button[1],onClickBtnReg)
end
addEvent("openRegistration",true)
addEventHandler("openRegistration",getRootElement(),openRegisterGUI)

function onClickBtnReg(button,state)
if (button == "left" and state == "up") then
    if (source == GUIEditorr.button[1]) then
	 username = guiGetText(GUIEditorr.edit[1])
	 password = guiGetText(GUIEditorr.edit[2])
	 passwordConfirm = guiGetText(GUIEditorr.edit[3])
	 triggerServerEvent("onRequestRegister",getLocalPlayer(),username,password,passwordConfirm)
	 triggerServerEvent("submitLogin",getLocalPlayer(),username,password)
end
end
end
function reg1(button,state)
     guiSetInputEnabled(false)
	 guiSetVisible(GUIEditorr.window[1],false)
	 showCursor(false)
	 destroyElement(GUIEditorr.window[1])
  end
addEvent("closeRegg",true)
addEventHandler("closeRegg",getRootElement(),reg1)
function reg2(button,state)
if (button == "left" and state == "up") then
   if (source == GUIEditorr.button[1]) then
   	guiSetInputEnabled(false)
	guiSetVisible(GUIEditorr.window[1], true)
	showCursor(true)
end
end
end
addEvent("closeReg",true)
addEventHandler("closeReg",getRootElement(),reg2)
function onClickBtnBack(button,state)
if (button == "left" and state == "up") then
   if (source == GUIEditorr.button[2]) then
       triggerServerEvent("openClienty",getLocalPlayer())
	   guiSetInputEnabled(true)
	   guiSetVisible(GUIEditorr.window[1],false)
	end
  end
end