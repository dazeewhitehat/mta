GUIEditor = {
    gridlist = {},
    window = {},
    button = {},
    label = {}
}
    function spawnOpen()
        GUIEditor.window[2] = guiCreateWindow(507, 161, 353, 447, "Spawn selection menu", false)
        guiWindowSetSizable(GUIEditor.window[2], false)
        guiSetAlpha(GUIEditor.window[2], 0.85)

        GUIEditor.gridlist[2] = guiCreateGridList(10, 31, 329, 261, false, GUIEditor.window[2])
        guiGridListAddColumn(GUIEditor.gridlist[2], "Spawn Menu!", 0.9)
        for i = 1, 3 do
            guiGridListAddRow(GUIEditor.gridlist[2])
        end
        guiGridListSetItemText(GUIEditor.gridlist[2], 0, 1, "LS", false, false)
        guiGridListSetItemText(GUIEditor.gridlist[2], 1, 1, "SF", false, false)
        guiGridListSetItemText(GUIEditor.gridlist[2], 2, 1, "LV", false, false)
        GUIEditor.button[2] = guiCreateButton(22, 375, 50, 27, "", false, GUIEditor.gridlist[2])

        GUIEditor.button[2] = guiCreateButton(10, 302, 107, 34, "Spawn", false, GUIEditor.window[2])
        GUIEditor.button[3] = guiCreateButton(237, 302, 102, 34, "Cancel", false, GUIEditor.window[2])
        GUIEditor.label[1] = guiCreateLabel(24, 336, 246, 102, "Description of the selected city:", false, GUIEditor.window[2])
		showCursor(not isCursorShowing())
end
addEvent("openSpawnMenu",true)
addEventHandler("openSpawnMenu",getRootElement(),spawnOpen)

addCommandHandler("spawn",spawnOpen)
function closeGUI (state)
 if state == "left" or "right" then
  if source == GUIEditor.button[3] then
   guiSetVisible(GUIEditor.window[2], false)
   showCursor(false)
   setElementData(source,"temp2.selected1",false)
   setElementData(source, "temp2.selected2",false)
   setElementData(source, "temp2.selected3",false)
   end
  end
 end
addEventHandler("onClientGUIClick", getRootElement(), closeGUI)
function click (state)
if state == "left" then
  if source == GUIEditor.button[2] then
       local spawnMenu = guiGridListGetItemText ( GUIEditor.gridlist[2], guiGridListGetSelectedItem ( GUIEditor.gridlist[2] ), 1 )
	if spawnMenu == nil or spawnMenu == false then
	outputChatBox("You need to select a city!",255,255,0)
	elseif spawnMenu == "LS" then
       outputChatBox("You have selected and spawned in Los Santos!",0,255,0)
	   guiSetVisible(GUIEditor.window[2],false)
	   thePlayer = getLocalPlayer()
	   showCursor(false)
	   triggerServerEvent("spawnLS",getLocalPlayer(),thePlayer,spawn1)
	   setElementData(source,"temp.spawnmenu",0)
	elseif spawnMenu == "SF" then
	   outputChatBox("You have selected and spawned in San Fierro!",0,255,0)
	   guiSetVisible(GUIEditor.window[2],false)
	   thePlayer = getLocalPlayer()
	   showCursor(false)
	   triggerServerEvent("spawnSF",getLocalPlayer(),thePlayer,spawn2)
	   setElementData(source, "temp.spawnmenu",1)
	elseif spawnMenu == "LV" then
	   outputChatBox("You have selected and spawned in Las Venturas!",0,255,0)
       guiSetVisible(GUIEditor.window[2],false)
	   thePlayer = getLocalPlayer()
	   showCursor(false)
	   triggerServerEvent("spawnLV",getLocalPlayer(),thePlayer,spawn3)
	   setElementData(source, "temp.spawnmenu",2)
	   else 
	   outputChatBox("You need to select a city!",255,255,0)
	            end
            end
        end
    end
addEventHandler ( "onClientGUIClick", getRootElement(), click )
function updateLabel(state)
if state == "left" then
   if source == GUIEditor.gridlist[2] then
   local spawnMenu = guiGridListGetItemText ( GUIEditor.gridlist[2], guiGridListGetSelectedItem ( GUIEditor.gridlist[2] ), 1 )
   if getElementData(source,"temp2.selected1") == false and spawnMenu == "LS" then
   GUIEditor.label[2] = guiCreateLabel(10, 20, 247, 86, "Los Santos", false, GUIEditor.label[1])
   guiSetVisible(GUIEditor.label[3],false)
   guiSetVisible(GUIEditor.label[4],false)
   setElementData(source, "temp2.selected1",true)
   setElementData(source, "temp2.selected2",false)
   setElementData(source, "temp2.selected3",false)
  elseif getElementData(source, "temp2.selected1") == true and spawnMenu == "LS" then
  outputChatBox("Already selected!",255,0,0)
  guiSetVisible(GUIEditor.label[2],false)
  guiSetVisible(GUIEditor.label[2],true)
  guiSetVisible(GUIEditor.label[3],false)
  guiSetVisible(GUIEditor.label[4],false)
  elseif getElementData(source, "temp2.selected2") == false and spawnMenu == "SF" then
   GUIEditor.label[3] = guiCreateLabel(1, 22, 312, 80, "San Fierro", false, GUIEditor.label[1])
   guiSetVisible(GUIEditor.label[2],false)
   guiSetVisible(GUIEditor.label[4],false)
   setElementData(source, "temp2.selected2",true)
   setElementData(source, "temp2.selected3",false)
   setElementData(source, "temp2.selected1",false)
  elseif getElementData(source, "temp2.selected2") == true and spawnMenu == "SF" then
  outputChatBox("Already selected!",255,0,0)
  guiSetVisible(GUIEditor.label[3],false)
  guiSetVisible(GUIEditor.label[3],true)
  guiSetVisible(GUIEditor.label[4],false)
  guiSetVisible(GUIEditor.label[2],false)
  elseif getElementData(source, "temp2.selected3") == false and spawnMenu == "LV" then
  GUIEditor.label[4] = guiCreateLabel(1, 22, 312, 80, "Las Venturas", false, GUIEditor.label[1])
  guiSetVisible(GUIEditor.label[3],false)
  guiSetVisible(GUIEditor.label[2],false)
  setElementData(source, "temp2.selected3",true)
  setElementData(source, "temp2.selected2",false)
  setElementData(source, "temp2.selected1",false)
  elseif getElementData(source, "temp2.selected3") == true and spawnMenu == "LV" then
  outputChatBox("Already selected!",255,0,0)
  guiSetVisible(GUIEditor.label[4],false)
  guiSetVisible(GUIEditor.label[4],true)
  guiSetVisible(GUIEditor.label[3],false)
  guiSetVisible(GUIEditor.label[2],false)
    end
   end
  end
 end
addEventHandler("onClientGUIClick",getRootElement(),updateLabel)
function doubleClickedLabel(state)
if state == "left" or "right" then
if source == GUIEditor.gridlist[2] then
outputChatBox("You have double clicked!",255,0,0)
guiSetVisible(GUIEditor.label[2],false)
guiSetVisible(GUIEditor.label[3],false)
guiSetVisible(GUIEditor.label[4],false)
  end
 end
end
addEventHandler("onClientGUIDoubleClick",getRootElement(),doubleClickedLabel)
   ---unnecessary for testing purposes---
addCommandHandler( "cursor", 
    function ()
        -- Show the cursor if it is not showing or hide the cursor if it is
        showCursor( not isCursorShowing ( ) )
    end)
function openMenu(player)
guiSetVisible(GUIEditor.window[2],true)
showCursor(true)
end