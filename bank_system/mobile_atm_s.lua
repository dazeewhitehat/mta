function recieveDeposit(amount)
local money = getPlayerMoney(source)
local pAcc = getPlayerAccount(source)
if (tonumber(amount) == nil) then
outputChatBox("Please enter a value not a character/string!",source,0,255,255)
cancelEvent()
elseif (tonumber(amount) > tonumber(money)) then
outputChatBox("You can't deposit more money than you already have!",source,0,255,255)
cancelEvent()
elseif (tonumber(amount) == 0) then
outputChatBox("You can't deposit $0 dollars, silly you!",source,0,255,255)
cancelEvent()
elseif(tonumber(amount) == tonumber(money) or tonumber(amount) < tonumber(money)) then
outputChatBox("You have succesfully deposited: $" ..amount.. " into your bank account!",source,0,2500,0)
local takenMoney = takePlayerMoney(source,tonumber(amount))
local bankData = getAccountData(pAcc,"account.bank")
local convertedData = tonumber(bankData) + tonumber(amount)
local setData = setAccountData(pAcc,"account.bank",convertedData)
local bankData2 = getAccountData(pAcc,"account.bank")
local money2 = getPlayerMoney(source)
triggerClientEvent(source,"updateLabelData",source,bankData2)
triggerClientEvent(source,"updateLabelData2",source,money2)
end
end
addEvent("depositATM",true)
addEventHandler("depositATM",getRootElement(),recieveDeposit)
function withdrawMoney(amount)
local pAcc = getPlayerAccount(source)
if (amount == nil or not tonumber(amount)) then
outputChatBox("Please enter a value not a character/string!",source,0,255,255)
cancelEvent()
else
  local accountMoney = getAccountData(pAcc,"account.bank")
  local convertedMoney = tonumber(accountMoney)
  local lostMoney = tonumber(accountMoney) - tonumber(amount)
if (convertedMoney == nil) then
outputChatBox("Please enter a value not a character/string!",source,0,255,255)
cancelEvent()
elseif (amount == nil) then
outputChatBox("Please enter a value not a character/string!",source,0,255,255)
cancelEvent()
elseif (tonumber(amount) > convertedMoney) then
outputChatBox("You can't withdraw more money than you actually have in your bank account!",source,0,255,255)
cancelEvent()
elseif (tonumber(amount) == 0) then
outputChatBox("You can't withdraw $0 dollars, silly you!",source,0,255,255)
cancelEvent()
elseif (tonumber(amount) > 99999999) then
outputChatBox("You can't withdraw more than $99,999,999 because that is the maximum you can carry on hand!",source,0,255,255)
cancelEvent()
elseif (tonumber(amount) == convertedMoney or tonumber(amount) < convertedMoney) then
  outputChatBox("You successfully withdrew: $" ..amount.. " from your bank account!",source,0,2500,0)
  givePlayerMoney(source,tonumber(amount))
  setAccountData(pAcc,"account.bank",lostMoney)
  local bankData = getAccountData(pAcc,"account.bank")
  local onhandmoney = getPlayerMoney(source)
  triggerClientEvent(source,"updateLabelData",source,bankData)
  triggerClientEvent(source,"updateLabelData2",source,onhandmoney)
end
end
end
addEvent("withdrawATM",true)
addEventHandler("withdrawATM",getRootElement(),withdrawMoney)
function updateBalanceOnOpenGUI()
local pAcc = getPlayerAccount(source)
local onhandmoney = getPlayerMoney(source)
if (pAcc) then
local money = getAccountData(pAcc,"account.bank")
triggerClientEvent(source, "updateLabelData",source,money)
triggerClientEvent(source, "updateLabelData2",source,onhandmoney)
end
end
addEvent("updateBalanceOnGUI",true)
addEventHandler("updateBalanceOnGUI",getRootElement(),updateBalanceOnOpenGUI)
function resetAccount(thePlayer)
local pAcc = getPlayerAccount(thePlayer)
if (pAcc) then
setAccountData(pAcc,"account.bank",0)
end
end
addCommandHandler("resetbank",resetAccount) -- testing purposes--