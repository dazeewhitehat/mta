
ATMGUI = {
    button = {},
    window = {},
    label = {},
    edit = {}
}

    function openATMGUI()
        ATMGUI.window[1] = guiCreateWindow(479, 133, 467, 511, "MOBILE|ATM", false)
        guiWindowSetSizable(ATMGUI.window[1], false)
        guiSetAlpha(ATMGUI.window[1], 0.90)
        ATMGUI.button[1] = guiCreateButton(25, 111, 119, 42, "Deposit", false, ATMGUI.window[1])
        ATMGUI.button[2] = guiCreateButton(181, 111, 106, 42, "Withdraw", false, ATMGUI.window[1])
        ATMGUI.edit[1] = guiCreateEdit(105, 42, 235, 23, "", false, ATMGUI.window[1])
        ATMGUI.label[1] = guiCreateLabel(188, 24, 53, 18, "Amount:", false, ATMGUI.window[1])
        guiSetFont(ATMGUI.label[1], "default-bold-small")
        ATMGUI.label[2] = guiCreateLabel(10, 75, 374, 18, "In the box above enter the amount you wish to deposit/withdraw!", false, ATMGUI.window[1])
        guiSetFont(ATMGUI.label[2], "default-bold-small")
        ATMGUI.label[3] = guiCreateLabel(15, 175, 124, 18, "Your current balance:", false, ATMGUI.window[1])
        guiSetFont(ATMGUI.label[3], "default-bold-small")
        ATMGUI.label[4] = guiCreateLabel(149, 175, 288, 18, "", false, ATMGUI.window[1])
        ATMGUI.button[3] = guiCreateButton(323, 111, 114, 42, "Close", false, ATMGUI.window[1])
		ATMGUI.label[5] = guiCreateLabel(340, 21, 122, 15, "Your on-hand cash:", false, ATMGUI.window[1])
        guiSetFont(ATMGUI.label[5], "default-bold-small")
        ATMGUI.label[6] = guiCreateLabel(360, 36, 77, 19, "", false, ATMGUI.window[1])    
        showCursor(true)
		guiSetInputEnabled(true)
		guiSetProperty(ATMGUI.edit[1], "ValidationString", "^[0-9]*$")
		triggerServerEvent("updateBalanceOnGUI",getLocalPlayer())
        addEventHandler("onClientGUIClick",ATMGUI.button[3],onPlayerClickBtnClose)
		addEventHandler("onClientGUIClick",ATMGUI.button[1],onPlayerClickBtnDeposit)
		addEventHandler("onClientGUIClick",ATMGUI.button[2],onPlayerClickBtnWithDraw)
    end	
addCommandHandler("mobileatm",openATMGUI)
function comma_value(amount)
  local formatted = amount
  while true do  
    formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
    if (k==0) then
      break
    end
  end
  return formatted
end
function onPlayerClickBtnClose(button,state)
if (button == "left" and state == "up") then
    if (source == ATMGUI.button[3]) then
	   guiSetInputEnabled(false)
       guiSetVisible(ATMGUI.window[1],false)
	   showCursor(false)
	   destroyElement(ATMGUI.window[1])
       end
    end
end
function onPlayerClickBtnDeposit(button,state)
    if (button == "left" and state == "up") then
        if (source == ATMGUI.button[1]) then
	     local amount = guiGetText(ATMGUI.edit[1])
		 triggerServerEvent("depositATM",getLocalPlayer(),amount)
        end
    end
end
function onPlayerClickBtnWithDraw(button,state)
if (button == "left" and state == "up") then
    if (source == ATMGUI.button[2]) then
	   local amount = guiGetText(ATMGUI.edit[1])
	   triggerServerEvent("withdrawATM",getLocalPlayer(),amount)
	   end
	end
end
function updateBalanceLabel(convertedMoney,onHandMoney)
if (convertedMoney) then
guiSetText(ATMGUI.label[4], "$" ..comma_value(tostring(convertedMoney)).. "")
end
end
addEvent("updateLabelData",true)
addEventHandler("updateLabelData",getRootElement(),updateBalanceLabel)
function updateBalanceLabel2(onHandMoney)
if (onHandMoney) then
guiSetText(ATMGUI.label[6], "$" ..comma_value(tostring(onHandMoney).. ""))
end
end
addEvent("updateLabelData2",true)
addEventHandler("updateLabelData2",getRootElement(),updateBalanceLabel2)