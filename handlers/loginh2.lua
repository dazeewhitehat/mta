local LS = createColRectangle (-800.81671, -2990.10657, 5100, 3330)
local LV = createColRectangle (-810, 350, 5000, 3000)
local SF = createColRectangle (-4000, -3000, 3190, 6000)
function getPlayerCityName(element)
if ( not isElement(element) ) then return end
    if (isElementWithinColShape(element, LS)) then
        result = "LS"
    elseif (isElementWithinColShape(element, LV)) then
        result = "LV"
    elseif (isElementWithinColShape(element, SF)) then
        result = "SF"       
    end
    return result
end
addEventHandler("onPlayerWasted",getRootElement(),
function()
---SF HOSPITAL X,Y,Z---
sfHospitalX = -2660.66
sfHospitalY = 635.05
sfHospitalZ = 14.45
---LV HOSPITAL X,Y,Z---
lvHospitalX = 1606.95
lvHospitalY = 1821.21
lvHospitalZ = 10.82
--LS ALL SAINTS HOSPITAL X,Y,Z--
lsHospital1X = 1177.16
lsHospital1Y = -1323.70
lsHospital1Z = 14.05
--LS JEFFERSON HOSPITAL X,Y,Z--
lsHospital2X = 2032.85
lsHospital2Y = -1416.39
lsHospital2Z = 16.99
local playeraccount = getPlayerAccount(source)
  if getPlayerCityName(source) == "SF" then
     local moneyAcc = getAccountData(playeraccount, "account.money")
	 setTimer(sfSpawnDelay, 2000, 1, source)
	 outputChatBox("You have respawned at the San Fierro hospital!",source,0,255,0)
	 setPlayerMoney(source, moneyAcc)
   elseif getPlayerCityName(source) == "LV" then
     local moneyAcc2 = getAccountData(playeraccount, "account.money")
	 setTimer(lvSpawnDelay, 2000,1, source)
	 outputChatBox("You have respawned at the Las Venturas hospital!",source,0,255,0)
	 setPlayerMoney(source, moneyAcc2)
   elseif getElementZoneName(source) == "Market" then
     local moneyAcc3 = getAccountData(playeraccount, "account.money")
	 setTimer(lsSpawnDelay, 2000,1,source)
	 outputChatBox("You have respawned at the All Saints hospital in LS!",source, 0,255,0)
	 setPlayerMoney(source,moneyAcc3)
   elseif getElementZoneName(source) == "Jefferson" or "Idlewood" or "Glen Park" then
     local moneyAcc4 = getAccountData(playeraccount, "account.money")
	 setTimer(lsSpawnDelay, 2000, 1, source)
	 outputChatBox("You have respawned at the Jefferson hospital in LS!",source,0,255,0)
	 setPlayerMoney(source,moneyAcc4)
   elseif getPlayerCityName(source) == "LS" then 
   local randomLS = math.random(1,2)
    if randomLS == 1 then
	setTimer(lsSpawn1, 2000, 1,source)
	elseif randomLS == 2 then
	setTimer(lsSpawn2, 2000, 1,source)
   end
  end
end)
function lsSpawnDelay(player)
if (isElement(player)) then
 if getElementZoneName(player) == "Jefferson" or "Idlewood" or "Glen Park" then
   fadeCamera(player,true,0.5)
   spawnPlayer(player, lsHospital2X,lsHospital2Y,lsHospital2Z)
   setCameraTarget(player)
 elseif getElementZoneName(player) == "Market" or "Downtown Los Santos" then
   fadeCamera(player,true,0.5)
   spawnPlayer(player,lsHospital1X,lsHospital1Y,lsHospital1Z)
   setCameraTarget(player)
    end
  end
end
function lsSpawn1(player)
if(isElement(player)) then
  fadeCamera(player,true,0.5)
  spawnPlayer(player,lsHospital1X,lsHospital1Y,lsHospital1Z)
  setCameraTarget(player)
  end
end
function lsSpawn2(player)
if(isElement(player)) then
  fadeCamera(player,true,0.5)
  spawnPlayer(player,lsHospital2X,lsHospital2Y,lsHospital2Z)
  setCameraTarget(player)
  end
end
function sfSpawnDelay(player)
if (isElement(player)) then
   fadeCamera(player,true,0.5)
   spawnPlayer(player, sfHospitalX,sfHospitalY,sfHospitalZ)
   setCameraTarget(player)
   end
end
function lvSpawnDelay(player)
if (isElement(player)) then
   fadeCamera(player,true,0.5)
   spawnPlayer(player,lvHospitalX,lvHospitalY,lvHospitalZ)
   setCameraTarget(player)
   end
end
function loopCitiesForScoreboard()
for i, player in ipairs(getElementsByType("player")) do
local playeraccount = getPlayerAccount(player)
if isGuestAccount(playeraccount) then return end
   if getPlayerCityName(player) == "LS" then
   lsString = "LS"
   setElementData(player,"City", lsString)
   elseif getPlayerCityName(player) == "SF" then
   sfString = "SF"
   setElementData(player,"City",sfString)
   elseif getPlayerCityName(player) == "LV" then
   lvString = "LV"
   setElementData(player, "City",lvString)
   end
  end
end
setTimer(loopCitiesForScoreboard,500,0)