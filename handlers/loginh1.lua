addEventHandler("onPlayerLogin",getRootElement(),
function()
    local pAcc = getPlayerAccount(source)
	local civTeam = getTeamFromName("Civilian")
	local playerName = getPlayerName(source)
    local playerFromName = getPlayerFromName(playerName)
    if not getAccountData(pAcc, "first.time") then
	     outputChatBox("Welcome to the server, this seems like your first time!",source,0,255,0)
		 outputChatBox("Choose a city!",source,255,255,0)
		 setAccountData(pAcc,"first.time",true)
		 setPlayerTeam(source, civTeam)
		 givePlayerMoney(playerFromName,15000)
		 exports.tpmenu:triggerMenu(playerFromName)
		 exports.stats:triggerDefaultsForFirstLogin(playerFromName)
		 setAccountData(pAcc, "account.walkstyle",0)
		 setAccountData(pAcc, "account.bank",0)
		else
        local loginX = getAccountData(pAcc, "loginlocation.x")
        local loginY = getAccountData(pAcc, "loginlocation.y")
        local loginZ = getAccountData(pAcc, "loginlocation.z")
		local lastWalk = getAccountData(pAcc, "account.walkstyle")
		local accountMoney = getAccountData(pAcc, "account.money")
		local lastTeam = getAccountData(pAcc, "last.team")
		spawnPlayer(source,loginX,loginY,loginZ)
		setElementFrozen(source,false)
		if (accountMoney) then	
		   setPlayerMoney(source, accountMoney)
        end
		if (lastTeam) then
		local getTheTeamsName = getTeamFromName(lastTeam)
		setPlayerTeam(source,getTheTeamsName)
		else
		setPlayerTeam(source,"Civilian")
		cancelEvent()
		end
		if getAccountData(pAcc,"account.walkstyle") == nil then
		outputChatBox("NIL WALKSTYLE")
		elseif getAccountData(pAcc, "account.walkstyle") == 0 then
		setPedWalkingStyle(source,0)
		elseif getAccountData(pAcc, "account.walkstyle") == 1 then
		setPedWalkingStyle(source,69)
		elseif getAccountData(pAcc, "account.walkstyle") == 2 then
		setPedWalkingStyle(source,128)
		end
    end
    fadeCamera(source, true)
    setCameraTarget(source, source)
    outputChatBox("You are now logged in!, Welcome",source,0,255,0)
end)

addEventHandler("onPlayerQuit",getRootElement(),
function()
    local pAcc = getPlayerAccount (source)
	local getHisTeam = getPlayerTeam(source)
	local playersMoney = getPlayerMoney(source)
	local lastWalk = getElementData(source, "temp.walkOpen")
    if (pAcc) and not isGuestAccount(pAcc) then
        local lastOccupation = getAccountData(pAcc, "account.occupation")
        local logoutX, logoutY, logoutZ = getElementPosition(source)
        setAccountData(pAcc, "loginlocation.x", logoutX)
        setAccountData(pAcc, "loginlocation.y", logoutY)
        setAccountData(pAcc, "loginlocation.z", logoutZ)
		setAccountData(pAcc, "marked.latest.temp", false)
		setAccountData(pAcc, "account.occupation",lastOccupation)
	    setAccountData(pAcc, "account.money", playersMoney)
		setAccountData(pAcc, "account.walkstyle", lastWalk)
	if (getHisTeam) then
	    local teamsName = getTeamName(getHisTeam)
		setAccountData(pAcc,"last.team", teamsName)
     end
   end
end)
---ALL BELOW HERE IS FOR TESTING PURPOSES---
function resetLogin(thePlayer,cmdname,arg3)
local playerName = getPlayerName(thePlayer)
local playerFromName = getPlayerFromName(playerName)
local pAcc = getPlayerAccount(playerFromName)
setAccountData(pAcc, "first.time",false)
outputChatBox("Reseted the login!",playerFromName,0,255,0)
end
addCommandHandler("rl",resetLogin)
function checkWalk(thePlayer,cmdname,arg3)
if (thePlayer) then
 local pAcc = getPlayerAccount(thePlayer)
 local lastWalk = getAccountData(pAcc, "account.walkstyle")
 outputChatBox("Result: " ..lastWalk.. " ",thePlayer,0,255,0)
end
end
function setSneak(thePlayer,cmdname,arg3)
if (thePlayer) then
local pAcc = getPlayerAccount(thePlayer)
setAccountData(pAcc,"account.walkstyle",arg3)
outputChatBox("DONE",thePlayer)
end
end
addCommandHandler("cwalk",checkWalk)
addCommandHandler("ss",setSneak)
function checkData(thePlayer)
local pAcc = getPlayerAccount(thePlayer)
if (pAcc) then
local theData = getAccountData(pAcc, "account.interior")
outputChatBox("Result:" ..theData.. " ",thePlayer,0,255,0)
end
end
function setData(thePlayer,cmdname,arg3)
local pAcc = getPlayerAccount(thePlayer)
if (pAcc) then
setAccountData(pAcc, "account.interior",arg3)
end
end
function convertData(thePlayer)
local pAcc = getPlayerAccount(thePlayer)
if (pAcc) then
local retrievedData = getAccountData(pAcc, "account.interior")
local convertedData = tonumber(retrievedData)
setAccountData(pAcc,"account.interior",convertedData)
end
end
addCommandHandler("c1",checkData)
addCommandHandler("c2",setData)
addCommandHandler("c3",convertData)