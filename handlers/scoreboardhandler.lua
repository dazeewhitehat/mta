exports.scoreboard:addScoreboardColumn('Current Occupation',source,2,125)
exports.scoreboard:addScoreboardColumn('WL',source,2,20)
exports.scoreboard:addScoreboardColumn('City',source,3,20)
exports.scoreboard:addScoreboardColumn('Cash') 
function comma_value(amount)
  local formatted = amount
  while true do  
    formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
    if (k==0) then
      break
    end
  end
  return formatted
end
function currentMoney ( )
 for i, player in ipairs(getElementsByType("player")) do
local playeraccount = getPlayerAccount(player)
if isGuestAccount(playeraccount) then return end
    setElementData( player,"Cash", "$" ..comma_value(getPlayerMoney(player).. ""))
  end
end
function currentOccupation()
for i, player in ipairs(getElementsByType("player")) do
local playeraccount = getPlayerAccount(player)
if isGuestAccount(playeraccount) then return end

   local playeraccount = getPlayerAccount(player)
   local occupationAccData = getAccountData(playeraccount, "account.occupation")
   setElementData(player,"Current Occupation",occupationAccData)
   end
end
setTimer(currentMoney, 5000, 0)
setTimer(currentOccupation,5000,0)
addEventHandler("onPlayerLogin",root,currentMoney)
addEventHandler("onPlayerLogin",root,currentOccupation)