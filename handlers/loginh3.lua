function onDeath()                                          -- SAVE SKIN WHEN YOU DIE--
      local pAcc = getPlayerAccount ( source )
      if ( pAcc ) then
            local playerskin = getPedSkin ( source )
            setAccountData ( pAcc, "account.skin", playerskin )
      end
end
function onSpawn()                                             --- WHEN YOU SPAWN RETAIN SAVED SKIN--
      local pAcc = getPlayerAccount ( source )
      if ( pAcc ) then
            local playerskin = getAccountData ( pAcc, "account.skin" )
			if (playerskin) then
                  setPedSkin ( source, playerskin )
				  else
				  cancelEvent()
            end
      end
end
addEventHandler("onPlayerWasted",getRootElement(),onDeath)
addEventHandler("onPlayerQuit",getRootElement(),onDeath)
addEventHandler("onPlayerLogin",getRootElement(),onSpawn)