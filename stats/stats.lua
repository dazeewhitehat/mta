validargtable = {"briefcase","bd"}
function totalBfCases(thePlayer)
local pAcc = getPlayerAccount(thePlayer)
if (pAcc) then
local totalDelivered = getAccountData(pAcc, "acc.stats.briefcase")
if totalDelivered == nil or not totalDelivered then
outputChatBox("You haven't delivered any of the briefcases!",thePlayer,0,255,255)
else
outputChatBox("Total briefcases delivered: " ..totalDelivered.. " ",thePlayer,0,255,0)
end
end
end
function triggerDefaultsForFirstLogin(thePlayer) --- THIS IS TRIGGERED FROM THE FIRST HANDLER IF THE PLAYER NEVER LOGGED IN BEFORE AND THIS IS THE FIRST TIME SO THEIR STATS ARE SET TO 0 AND ARE NOT NIL----
local pAcc = getPlayerAccount(thePlayer)
if (pAcc) then
setAccountData(pAcc, "acc.stats.briefcase", 0) --BRIEFCASE DEFAULT--
end
end
function quickStats(thePlayer,cmdname,arg3)
local pAcc = getPlayerAccount(thePlayer)
if arg3 == nil then
outputChatBox("You have to enter a stat name for something to come out LOL?!",thePlayer,0,255,255)
cancelEvent()
elseif arg3 == "briefcase" then
local totalDeliveredBriefCases = getAccountData(pAcc, "acc.stats.briefcase")
outputChatBox("Total briefcases you delivered is: " ..totalDeliveredBriefCases.. " ",thePlayer,0,255,0)
end
end
addCommandHandler("qs",quickStats)